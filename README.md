### What is this repository for? ###

This is a simple example project to show the proper use of the [run-in-background plugin](https://bitbucket.org/TheBosZ/cordova-plugin-run-in-background) for Cordova 

### How do I get set up? ###

1. Check out this project
1. Run `npx cordova platform add android`
1. Run `npx cordova run android`

### The magic formula is in [index.js](./www/js/index.js):

```javascript
var bg = window.cordova.plugins.backgroundMode;
bg.setDefaults({
	text: 'App is running in background',
	hidden: false,
	color: '0098D9',
	icon: 'power',
	allowClose: true,
	channelDescription: 'Keep the App running in the background',
	channelName: 'Keep running in background',
	subText: 'Small hint text',
	showWhen: false
});
bg.enable();
bg.on('activate', function () {
	bg.disableWebViewOptimizations();
});
bg.disableBatteryOptimizations();
bg.moveToBackground();
```

### Notes:

This readme is assuming that your environment is set up for Android development. If not, please follow the [Android Platform Guide](https://cordova.apache.org/docs/en/latest/guide/platforms/android/) for Cordova.